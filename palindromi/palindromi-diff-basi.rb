# numeri palindromi in più basi
# by p.p. 8giu18
#
maxnum, maxbase = 1000, 36
minnum, minbase = 11, 2
maxnp, maxn = 0, 0
conta = Array.new(maxbase,0)
(minnum .. maxnum).each do |n|
	np = 0
	(minbase .. maxbase).each do |b|
		nb = n.to_s(b).split ''
		next if nb != nb.reverse or nb.length == 1
		if np == 0 then print n, " palindromo in base:\n" end
		np += 1
		print  b, " ---> ", nb.join, "\n"
	end
	conta[np] +=1
	next if np == 0
	if np > 1 then print "numero di palindromi = ", np, "\n" end
	print "\n"
	if np > maxnp then maxnp = np; maxn = n end
end
print "\nmassimo numero di palindromi = ",maxnp," per n = ", maxn, "\n"
print "conta dei numeri di palindromi:\n", conta
