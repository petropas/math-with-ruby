# Is it possible to make a number and its square, using the digits from 1 trhough 9 exactly once?
# by p.p. 5 giugno 2018
#
n_min = (99999 ** 0.5).to_i + 1
n_max = 999
print "esploro da ", n_min, " a ", n_max, "\n"
(n_min .. n_max).each do |n|
	cifre = n.to_s.split('') + (n**2).to_s.split('')
	next if cifre.include? '0'
	next if cifre.length > cifre.uniq.length
	print "n = ", n, ", n^2 = ", n**2, "\n"
end

