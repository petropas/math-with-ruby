# Utilizzando tutte le cifre da 0 a 9, una sola volta: N, K e KN
# by p.p. 5 giugno 2018
#
n_max = 99999
print "esploro fino a ", n_max, "\n"
(2 .. 999).each do |k|
	(k .. n_max).each do |n|
		cifre = n.to_s.split('') + k.to_s.split('') + (k*n).to_s.split('')
		nc = cifre.length
		next if nc < 10
		break if nc > 10
		next if nc > cifre.uniq.length
		print "n = ", n, ", ", k, "n = ", k*n, "\n"
	end
end

