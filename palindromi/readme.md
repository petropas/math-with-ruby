#### Numeri in forma palindroma in più basi di numerazione:
- palindromi-diff-basi.rb
    - ricerca delle forme palindrome, per gli interi positivi fino a 1.000 (modificabile), nelle basi da 2 a 36 (limite non modificabile)
- palindromi-diff-basi.txt
    - stampa dei risultati del programma in Ruby

#### Utilizzo particolare delle cifre decimali:
- all-digits-1.rb
    - qual è il numero intero che, insieme al suo quadrato, utilizza tutte le cifre d 1 a 9, una volta sola?
- all-digits-1.txt
    - stampa dei risultati del programma in Ruby
- all-digits-7.rb
    - trovare due numeri che, insieme al loro prodotto, utilizzino tutte le cifre decimali da 0 a 9, una volta sola
- all-digits-7.txt
    - stampa dei risultati del programma in Ruby


Vai all'[articolo][iv-palindromi] su Inchiostrovirtuale.it

[iv-palindromi]:http://www.inchiostrovirtuale.it/numeri-palindromi/

##### Bibliografia:
- Pagina dei palindromi di [Ron Knott][Ron Knott]

[Ron Knott]:http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/IntegerReps/palindromes.html
