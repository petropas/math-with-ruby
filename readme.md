#### Raccolta di programmi in Ruby (e di qualche file excel) per la risoluzione di puzzle logico-matematici.

Ogni directory è dedicato a un tema specifico, e contiene un proprio file **readme.md** (formato [markdown][markdown]), a cui si rimanda per i dettagli sugli argomenti e i file della directory stessa.

Qui di seguito la macro-storia del progetto.

[markdown]:https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md


| **data** | **directory** | **descrizione dell'operazione** |
| :---: | :---: | :---: |
|        |       |       |
| 10giu18 | puzzle-logici | primo inserimento: puzzle di Henry Dudeney, puzzle dell'interrogatorio |
| 10giu18 | palindromi | primo inserimento: ricerca di palindromi in più basi, prodotti con particolari utilizzi delle cifre decimali |
| 9giu18 | math-with-ruby | inizio progetto |
