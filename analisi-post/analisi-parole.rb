# Analisi della frequenza delle parole di un testo
# by p.p. 30 giugno 2018, revisione del 7 luglio 2018
#
# post
# post = ["capitano", "nicole", "sophie", "lenna", "stuarda", "self-drive", "puzzle-week", "fumo", "palindromi", "terra-piatta"] # pasquale
# post = ["pitoti", "fate", "mille_miglia", "musica", "jon_snow", "formaggi_lombardi", "islanda", "titanic", "pipa", "finlandia" ] # lune
post = ["te_verde", "miele", "snsa", "aleppo", "foto_protezione", "evo", "geloni", "glifosato", "pollinosi", "ciliegie"] # jessi
# dettagli = 1 per stampare un dizionario per post; = 0 per la stampa di un unico dizionario
dettagli = 0
#
# stampa del dizionario utilizzato
def stampa_diz(diz)
	diz = diz.sort_by {|chiave, valore| [-valore, chiave]}.to_h
	diz.each do |chiave, valore|
		print chiave.to_s, ", ", valore, "\n"
	end
end
#
# costruzione del dizionario utilizzato
diz = Hash.new(0)
diz_length = Hash.new()
post.each do |titolo|
	if dettagli == 1 then diz.clear ; print "--> ", titolo, "\n" end
	File.open(titolo +".txt").each do |linea|
		parole = linea.force_encoding("UTF-8").downcase.split(/[[:punct:]\s[0-9]’°⁄×π]+/)
#		if parole.include?("g") then print titolo, ": ", linea, "\n" end
		parole.each do |singola|
			if singola != "" then diz[singola] += 1 end
		end
	end
	if dettagli == 1 then stampa_diz(diz) else diz_length[titolo] = diz.length end
end
if dettagli != 1 then
	print "dimensione dizionario accumulata:\n"
	post.each do |titolo|
		print titolo, ", ", diz_length[titolo], "\n"
	end
	print "\n\n--> all\n"
	stampa_diz(diz)
end
