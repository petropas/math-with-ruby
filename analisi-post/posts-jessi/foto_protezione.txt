L’estate è iniziata da un pezzo, è vero, ma non è mai troppo tardi per parlare di protezione solare!
La fotoprotezione, infatti, è indispensabile per limitare l’esposizione agli UV e le possibili conseguenze.

Limitare ma non evitare del tutto!
A piccole dosi, infatti, questi raggi sono fondamentali per il benessere psicofisico. Gli UVB, in particolare:

attivano la vitamina D, indispensabile per la salute delle ossa;
aumentano i livelli di endorfine e serotonina, esplicando un effetto antidepressivo naturale;
sono antisettici, per cui risultano utili in caso di acne;
stimolano le naturali difese della pelle, coadiuvati dagli UVA (ma questo lo vedremo più avanti).
Ed è per questo che, oggi, approfondiremo le strategie per attuarla a 360°. Perché non basta applicare il solare in spiaggia per proteggersi…

La fotoprotezione inizia a tavola!
Non è un caso che, tra gli alimenti considerati più salutari, vi siano: frutta, verdura e pesce azzurro. Questi alimenti, infatti, sono ricchi di sostanze che, assunte quotidianamente, preparano e proteggono la pelle dai raggi solari. Possiamo dividerle in due grandi categorie, in base all’azione svolta: gli antiossidanti e i grassi polinsaturi. I più curiosi potranno approfondire la questione a breve; i più pigri, invece, potranno passare direttamente agli alimenti cliccando qui.

Iniziamo dagli antiossidanti!
Quella degli antiossidanti è una categoria piuttosto variegata, includente principi nutritivi e non, che lavorano in sinergia per contrastare i radicali liberi. Questi ultimi, atomi o gruppi atomici particolarmente reattivi, attaccano i grassi, le proteine e il DNA delle cellule per trovare stabilità. Così facendo, però, le danneggiano, facendoci invecchiare più in fretta e aumentando il rischio di tumore. Benché i radicali vengano prodotti sempre, per via del metabolismo, l’esposizione agli UV ne incrementa notevolmente i livelli, rappresentando un pericolo per le zone esposte.

Ecco, quindi, che potrebbero comparire:

rughe, perché l’impalcatura di collagene ed elastina viene distrutta e poi riassemblata in modo anomalo;
macchie brune, perché la melanina (il pigmento dell’abbronzatura) viene ossidata e prodotta in modo non omogeneo;
lesioni precancerose (cheratosi attinica) e cancerose (melanoma, carcinoma squamo-cellulare e baso-cellulare), a causa della proliferazione incontrollata delle cellule cutanee.
Ma ora vediamoli, questi antiossidanti!

Carotenoidi
Si tratta dei pigmenti gialli, arancioni e rossi dei vegetali, che neutralizzano anche i radicali più pericolosi (ossigeno singoletto). Tra i più importanti vi sono:

luteina e zeaxantina per la salute degli occhi (proteggono la retina e, quindi, la vista);
β-carotene per la salute della pelle (previene l’eritema solare ed è utile in caso di porfiria);
licopene per un effetto protettivo globale (è un antiossidante di gran lunga più potente del β-carotene).
I carotenoidi sono responsabili del colore e delle proprietà dello zafferano. Un click qui per leggere l’articolo!

Vitamina C
Questa vitamina idrosolubile agisce su più fronti:

neutralizza i radicali liberi e rigenera la vitamina E danneggiata da questi ultimi;
stimola la produzione di collagene, elastina ed acido ialuronico, esplicando un effetto rassodante e antirughe;
aumenta l’utilizzo del selenio, importante per l’attività degli enzimi antiossidanti.
Sapevate che il coriandolo contiene tanta vitamina C? Ne parlo qui!

Vitamina E
Si tratta di una vitamina liposolubile che s’intercala nelle membrane cellulari (proteggendole dai radicali liberi) e rigenera il glutatione (un detossificante endogeno).

Acido lipoico
Altresì noto come vitamina N, l’acido lipoico:

rigenera la vitamina C e il glutatione ossidati;
chela i metalli coinvolti nella produzione dei radicali liberi;
neutralizza le specie reattive dell’azoto (tra i radicali più aggressivi).
Sali minerali
Sali minerali, quali: zinco, manganese, rame e selenio, sono fondamentali per il funzionamento degli enzimi antiossidanti.

Polifenoli
I polifenoli sono composti ubiquitari negli alimenti vegetali, ai quali conferiscono spiccate proprietà antiossidanti, antinfiammatorie e antitumorali.

Tra questi, come non citare la punicalagina del melograno e le catechine del tè verde?
O i biofenoli dell’olio extravergine d’oliva? O le antocianine delle ciliegie?

Passiamo, quindi, ai grassi polinsaturi!
Seppur con differenti meccanismi, anche i grassi insaturi contribuiscono alla fotoprotezione.

Gli omega-6, tra cui l’acido linoleico (LA), hanno un’azione restitutiva. Essi, infatti, vengono usati per produrre la matrice lipidica: una sorta di cemento che unisce le cellule epidermiche (come in un muro di mattoni) e previene le perdite d’acqua.
Gli omega-3 (ALA, EPA, DHA) modulano l’infiammazione e le risposte immunitarie. Essi infatti, sostituendosi all’acido arachidonico, vengono convertiti in molecole antinfiammatorie.
Tra le fonti di grassi polinsaturi vi è l’olio di Argan, di cui ho parlato qui!

Ma ora, veniamo al dunque: cosa mangiare per proteggerci dal sole?

Gli alimenti per una buona fotoprotezione
Ecco l’elenco degli alimenti che dovremmo consumare abitualmente, per preparare la pelle all’estate! Accanto agli alimenti di origine vegetale, troverete l’indice ORAC, che ne quantifica il potere antiossidante (maggiore è l’indice ORAC, più potente sarà l’alimento). Per una protezione adeguata, dovremmo assumere almeno 5.000 unità ORAC al giorno.

Verdura (ORAC riferiti a 100g)
Carciofo violetto (6.552)
Aglio bianco (5.346)
Barbabietola rossa (3.632)
Ravanello tondo (3.602)
Radicchio di Chioggia (3.537)
Broccolo (3.529)
Lattuga di Trento (3.323)
Spinacio (2.732)
Bietola (2.724)
Lattuga cocarde (2.127)
Cavolo nero (1.773)
Melanzana (1.414)
Asparago (1.288)
Peperone verde (1.059)
Lattuga catalogna (1.053)
Lattuga estiva (956)
Peperone giallo (950)
Lattuga romana (910)
Cavolo bianco (856)
Pomodoro San Marzano (697)
Peperoncino (534)
Porro (490)
Zucca gialla (396)
Pomodoro (395)
Cipolla (344)
Sedano (344)
Zucchina verde (344)
Cetriolo (182)
Carota (107)
Frutta (ORAC riferiti a 100g)
Spremuta di melagrana (6.030)
Succo di uva nera (5.216)
Mirtilli (3.480)
Prugne nere (1.454)
Pompelmo rosa (1.188)
Fragole (1.170)
Spremuta fresca di arancia (1.142)
Arancia (983)
Susina (626)
Avocado (571)
Uva nera (569)
Ciliegia (509)
Kiwi (458)
Uvetta nera (396)
Uva bianca (357)
Mela (301)
Pesca (248)
Banana (223)
Pera (222)
Melone (197)
Albicocche (172)
I dati provengono dal libro Dalle calorie alle molecole del Dottor Pier Luigi Rossi.

Alimenti ricchi di grassi polinsaturi
Riportiamo la quantità in milligrammi, di omega-3 e omega-6, per 100g di alimento (Russo, 2009).

Fotoprotezione - tabella che riporta gli alimenti ricchi di grassi polinsaturi
Gli alimenti ricchi di omega-3 e omega-6 (Russo, 2009)

Gli alimenti che garantiscono una fotoprotezione adeguata, dunque, sono numerosi. Basta solo mettersi un po’ d’impegno o, ancor meglio, rivolgersi ad un bravo nutrizionista che li inserisca in una dieta personalizzata.

Ma sapete qual è la buona notizia?
Che, consumandoli abitualmente, non avrete bisogno degli integratori per l’abbronzatura. Come dice il nome stesso, infatti, gli integratori sopperiscono alle carenze che – salvo eccezioni – non si vengono a creare in chi mangia con criterio! In ogni caso, prima di assumere qualsiasi integratore, dovreste sempre chiedere consiglio al medico o altro specialista della salute.

Ma ora veniamo a quello che, forse, è l’aspetto più noto della fotoprotezione.

Le regole per proteggersi dal sole
Ne avrete fin sopra i capelli di sentire la solita pappardella ogni estate, tuttavia, per una questione di scrupolo, non possiamo esimerci dal parlarne.

Quali sono, dunque, le regole per una buona fotoprotezione?
In casa, tenete abbassate le tapparelle quando il sole è alto.
Evitate di uscire nelle ore più calde (generalmente dalle 11:00 alle 16:00).
Prima di uscire, date un’occhiata al bollettino meteo, per avere indicazioni sull’Indice UV.
Ma cos’è l’Indice UV?
È un parametro che misura i livelli di UV sulla superficie terrestre. Viene espresso come intensità massima prevista nella giornata, per le diverse località o aree geografiche, e assume valori compresi tra 1 e 11. Ma vediamo il significato dei diversi valori!

1 e 2: si può stare all’aperto senza protezione;
da 3 a 7: richiede l’uso di una protezione chimica (solare) e fisica (cappello, occhiali e maglietta) nelle ore più calde;
da 8 a 11: non state all’aperto nelle ore centrali e state all’ombra in tutte le altre. Se proprio necessario, esponetevi solo dopo esservi muniti di protezione.
Indossate sempre occhiali scuri, cappello con visiera e indumenti chiari (larghi ed in tessuto naturale, per consentire la traspirazione).
Portate dell’acqua sempre con voi e bevetene almeno 2L al giorno, per evitare la disidratazione.
Non esponete alla luce diretta i bambini minori di 1 anno, più a rischio di tumore rispetto agli altri.
Esponetevi gradualmente, in modo che la pelle sviluppi le sue difese naturali.
A piccole dosi, infatti, gli UV stimolano:
l’ossidazione e la sintesi di melanina, che attenua le radiazioni e neutralizza i radicali liberi;
l’ispessimento dell’epidermide, che riduce la penetrazione delle radiazioni;
la secrezione di acido urocanico col sudore, che le attenua ulteriormente. 
Grazie a tutti questi processi, la pelle acquisisce un fattore di protezione pari a 4.

Per quanto riguarda i solari, invece…
Applicateli sempre, che siate in spiaggia, in montagna o in città, che il cielo sia terso o nuvoloso. Gli UV, infatti, sono più intensi in montagna rispetto al livello del mare e attraversano le nuvole.
Effettuate l’applicazione anche sotto i vestiti o il costume (una parte seppur piccola di UV attraversa i tessuti).
Applicateli almeno 30′ prima di esporvi al sole, in modo omogeneo ma senza farli assorbire del tutto, per avere una maggior protezione.
Rinnovate l’applicazione dopo che fate il bagno o che sudate, in ogni caso ogni 2-3 ore.
Vorreste saperne di più sui solari? Tranquilli, ne parlo qui!
Se vi foste già scottati, invece, cliccate qui e qui per rimediare!

E con questo è tutto, cari lettori, alla prossima!

L’articolo ha uno scopo puramente illustrativo e non sostituisce il parere del medico.
Bibliografia e sitografia
Se non espressamente indicato, le informazioni provengono da “Integratori alimentari e benessere cutaneo”  di SANITANOVA.