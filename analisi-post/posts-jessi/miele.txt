Fra i tesori dell’alveare è quello più prezioso: stiamo parlando del miele! Unico ed inimitabile sotto qualsiasi punto di vista, il miele apporta dei benefici che vanno ben oltre quelli nutrizionali e ne giustificano l’impiego nella pratica sportiva, in cosmesi e in fitoterapia. Volete saperne di più? Allora, seguitemi!
ape-da-miele-sopra-un-fiore-apis-mellifera
Apis mellifera, meglio nota come “ape da miele”.

Il miele (dall’ittita melit) è un liquido viscoso e zuccherino prodotto da Apis mellifera, a partire dal nettare o dalla melata. La produzione inizia nel momento in cui l’ape bottinatrice, fatto scorta di provviste, le digerisce nell’ingluvie o borsa melaria. Giunta nell’arnia, quindi, passa il testimone alle operaie che, raggiunta la qualità desiderata, ripartiscono il miele nelle cellette e lo concentrano, per evitare che fermenti. Terminato il duro lavoro delle api, non rimane che estrarre il miele dai favi, separarlo dalle impurità e confezionarlo.

Ma il miele è tutto uguale? Assolutamente no!
Le caratteristiche del prodotto, siano esse organolettiche o fitochimiche, variano in base all’origine delle materie prime: il nettare e la melata.

Il nettare e la melata sono liquidi zuccherini, prodotti rispettivamente dai fiori e da alcuni insetti (afidi e cocciniglie).
Le api possono prelevarne uno o più tipi, ragion per cui si possono ottenere i mieli unifloreali o quelli multifloreali. Il millefiori, ottenuto dal nettare di più specie botaniche, col suo colore ambrato e il sapore molto dolce è il miele per antonomasia: piace a tutti, adulti e bambini, ed è economico, fattori che contribuiscono alla sua ampia diffusione.

Al contrario, i mieli unifloreali vengono ottenuti da una singola specie, sono generalmente più costosi e possono differire notevolmente dal miele comune, il che li rende spesso dei prodotti di nicchia. Tra questi come posso non citare il miele di corbezzolo, tipico della mia amata Sardegna? Caratterizzato da un sapore molto intenso, amaro ed astringente – quindi, non adatto a tutti i palati! – questo miele ben si sposa col caffè, crea un piacevole contrasto coi formaggi dolci e viene usato per preparare un torrone speciale; inoltre, poiché la pianta fiorisce in autunno/inverno (periodo durante il quale le api faticano a prepararlo), il miele di corbezzolo può avere un prezzo fino ad otto volte superiore rispetto agli altri!

Ma cosa contiene il miele? Lo vediamo subito!

Valori nutrizionali e principi attivi.
Come abbiamo anticipato il miele è un alimento unico, la cui composizione non può essere riprodotta artificialmente, poiché varia in funzione dell’origine botanica e del lavoro delle api. In linea di massima, 100g di miele forniscono 304 Kcal e contengono:

zuccheri (80%), soprattutto fruttosio e glucosio, responsabili di dolcezza e viscosità;
acqua (17%);
aminoacidi (prolina), vitamine (B, C, K) e sali minerali (potassio, calcio, magnesio, fosforo e sodio);
acido gluconico ed altri acidi organici, che gli conferiscono un retrogusto acidulo;
enzimi (glucosio ossidasi, invertasi e diastasi), fondamentali per la digestione di nettare e melata;
antiossidanti, dei quali i mieli scuri sono particolarmente ricchi. Si tratta per lo più di acidi fenolici e flavonoidi, tra cui la pinocembrina, peculiare di miele e propoli.
Per maggiori informazioni, vi rimando alle tabelle 1 e 2 riportate su Nutrion & Metabolism (2012).
Ciascuno di questi componenti contribuisce a renderlo speciale, un alimento che non può di certo mancare nella vita di tutti i giorni. Scopriamo il perché!

Previene le patologie cardiovascolari. 
Grazie ai preziosi antiossidanti, il miele protegge il cuore e i vasi sanguigni dai radicali liberi. In questo modo abbassa la pressione, previene l’ossidazione del colesterolo cattivo e la formazione dei trombi, effetti che contrastano lo sviluppo dell’ipertensione, dell’aterosclerosi e delle coronaropatie (Erejuwa et al., 2012; Eteraf-Oskouei and Najafi, 2013).

Previene il diabete. 
Sembra un paradosso, visto che è composto soprattutto da zuccheri, ma recenti studi mostrano che il miele contrasta lo sviluppo del diabete! Questi benefici sono ascrivibili sia agli antiossidanti, che proteggono il pancreas dai radicali liberi e aumentano la sensibilità dei tessuti all’insulina, sia al fruttosio, che regola il metabolismo dei carboidrati (Erejuwa et al., 2012).

È neuroprotettivo e migliora le facoltà mentali.
Gli antiossidanti del miele, proteggendo i neuroni dai radicali liberi, migliorano la memoria e prevengono le convulsioni, oltre ad essere ansiolitici ed antidepressivi (Mijanur Rahman et al., 2014).

È antisettico. 
Studi dimostrano le potenzialità del miele contro l’Herpes simplex, i dermatofiti e i batteri, benché gli effetti su questi ultimi siano molto più marcati. Pensate, infatti, che è attivo contro ben 60 specie patogene, che possono infettare la cute, il tratto digerente, le vie respiratorie e quelle urinarie! Come riportato da Eteraf-Oskouei and Najafi, queste proprietà si esplicano attraverso:

la disidratazione dei batteri e l’acidificazione del pH, grazie agli zuccheri e agli acidi organici;
la produzione di acqua ossigenata, potente antisettico, ad opera dei suoi enzimi;
l’azione antiadesiva, dovuta probabilmente ai flavonoidi, che impedisce ai batteri di aggrapparsi alle mucose e colonizzarle.
Calma la tosse e lenisce la gola. 
Penso che tutti, almeno una volta, abbiate bevuto latte e miele, rimedio popolare contro la tosse. Uno studio di Cohen e colleghi, coinvolgente 300 bambini, ha dimostrato che il miele riduce la frequenza e la gravità della tosse notturna, migliorando così la qualità del sonno. Questi benefici sono ascrivibili sia agli zuccheri, che stimolando la deglutizione stroncano la tosse sul nascere, sia agli antiossidanti, che placano l’infiammazione della gola associata alla tosse. Grazie alle naturali proprietà antisettiche, inoltre, il miele impedisce ai microbi di colonizzare la mucosa faringea.

Nei bambini, la dose consigliata dall’OMS è di mezzo cucchiaino prima di andare a dormire, magari con latte caldo (Miceli et al., 2014).
Accelera la guarigione cutanea. 
Quando applicato sulla cute ferita o ustionata, il miele crea un ambiente ottimale per la guarigione delle lesioni. Come riportato da diversi autori (Al-Waili et al., 2011; Molan and Rhodes, 2015), infatti, il miele favorisce la cicatrizzazione in quanto:

disinfetta e lenisce la cute infiammata;
facilita la rimozione del tessuto necrotico;
infine, stimola la formazione di nuovi vasi sanguigni e la riepitelizzazione cutanea.
Pensate che queste proprietà sono state confermate da ben 26 studi clinici controllati, coinvolgenti più di 1.965 volontari (Molan et al., 2006; Aziz and Abdul Rasool Hassan, 2016)!

Migliora le prestazioni sportive. 
Secondo Ajibola e colleghi, il miele migliora le prestazioni ciclistiche meglio del glucosio! Esso, infatti, non solo è ricco di vitamine, potassio e magnesio (fondamentali per la produzione di energia e il corretto funzionamento dei muscoli), ma anche di antiossidanti, che proteggono il tessuto muscolare dallo stress ossidativo, e fruttosio, che possiede effetti positivi sul metabolismo degli zuccheri.

È idratante ed emolliente.
Ebbene si! Il miele rientra nella composizione di vari cosmetici, dai prodotti idratanti a quelli per la detersione, in quanto idrata ed ammorbidisce la pelle (Ediriweera and Premarathna, 2012). Un pregio notevole di questo prodotto, inoltre, è che non appesantisce le pelli grasse e acneiche, e le disinfetta. Non ci credete? Se amate il fai da te, ecco a voi 5 rimedi casalinghi che vi lasceranno soddisfatti!

Scrub viso. Macinate 5g di mandorle ed unitele a 5mL di miele; sfregate delicatamente la pelle col composto ottenuto e risciacquate con acqua tiepida.

Impacco idratante. Miscelate 2 cucchiai di miele e 2 cucchiaini di latte intero; applicate e lasciate in posa per 15′, dopodiché risciacquate con acqua tiepida.

Maschera levigante. Sbattete 1 albume d’uovo con 1 cucchiaio di miele, 1 cucchiaino di glicerina e farina d’avena (1/4 di tazza); applicate sul viso, lasciate in posa 15′ e risciacquate con acqua tiepida.

Maschera emolliente. Miscelate 1-2 cucchiai di miele con farina d’avena (1/3 di tazza) e 1 cucchiaino d’acqua alle rose; applicate sul viso e lasciate in posa per 15′, quindi, risciacquate con acqua tiepida.

Lozione per cute molto secca. Miscelate 5mL di miele, 5mL d’olio d’oliva e 2,5mL di succo di limone; applicate e lasciate in posa per 15′, dopodiché risciacquate con acqua tiepida.

Ma attenzione!
Nonostante i molteplici benefici, anche il miele può avere effetti negativi sulla salute! Esso, infatti, è controindicato nel primo anno di vita, in quanto potrebbe contenere spore di botulino o graianotossine, pericolose per i pargoli. Il consumo, inoltre, dovrebbe avvenire sotto controllo medico nei pazienti diabetici, a causa dell’elevato tenore zuccherino

Bene, cari amici, siamo giunti alla fine! Vi sareste mai aspettati che un alimento così comune possedesse tutte queste proprietà? Quelle che ho riportato sono solo alcune fra le più studiate! Concludo dicendo che l’articolo ha uno scopo puramente illustrativo. Per qualsiasi dubbio o curiosità, rivolgetevi al medico o altro specialista della salute.