#### Analisi di testi mediante un programma Ruby  

##### Le analisi effettuate  

Sono stati analizzati tre gruppi di 10 articoli ciascuno, dal blog [Inchiostro Virtuale][IV]. I testi analizzati sono nelle cartelle:

- posts  
- posts-lune  
- posts-jessi  

##### Gli aspetti valutati  

Ogni file excel riepilogativo contiene:  

- andamento della dimensione del dizionario utilizzato, con grafico dell'incremento post dopo post  
- grafico del numero di parole utilizzate in 1, 2, ... 10 post  
- andamento delle occorrenze di "connettivi", post per post, con relativa percentuale sul totale delle parole utilizzate in ogni post  
- insieme delle parole utilizzate in tutti e 10 i post, solo in 9, in 7 o 8 post  

##### Il programma  

La struttura del programma è abbastanza semplice: per ogni articolo di una lista, si isolano, linea per linea, le parole di testo. Successivamente si calcola, per ogni parola individuata, il numero di occorrenze sia nel singolo articolo che nell’insieme degli articoli della lista.  
Per individuare le singole parole si utilizza il metodo .split() che, applicato a una linea, ne pone in un array le singole parole presenti.  

Come si fa a estrarre le singole parole?  
Ci vengono in aiuto le regular expression, per istruire il programma su come individuare il separatore tra le singole parole.  
Separatori naturali sono gli spazi e i segni di interpunzione, ma vanno considerati separatori anche le cifre numeriche ed eventuali caratteri speciali utilizzati nei testi (ho aggiunto degli spazi per leggibilità):  

> split( / [ [:punct:] \s [0-9] ’ ° ⁄ × π ] + /)  

La coppia di parentesi quadre più esterna racchiude i possibili caratteri separatori: la classe dei segni di interpunzione, per fortuna predefinita, gli spazi (\s), le cifre numeriche ([0-9]). Va poi aggiunto l’apostrofo sexy di WordPress, inclinato all’indietro, oltre a una manciata di caratteri speciali che utilizzo di tanto in tanto.  
Il segno + finale indica che più caratteri separatori successivi vanno raggruppati in un solo separatore. Quindi, ad esempio, la linea:  

> Quante parole in questo testo? 5? 6? No, sette.  

considererà 2? 5? 6? " come un unico separatore, e così ", ", lasciando quindi esattamente 7 parole.  

Nota: l’algoritmo utilizzato è semplice ma rozzo nelle conclusioni. Un esempio: entrambe le grafie “nondimeno” e “non di meno” sono corrette, ma contribuiscono in modo decisamente diverso alla ricchezza del dizionario.  

##### L'esecuzione del programma  

L’hash di Ruby è una struttura dati che consente di associare dei valori a una stringa data, mediante una funzione di hash. Ad esempio:  

> diz = Hash.new(0)  

definisce “diz” come una struttura hash, inizializzata con il valore 0.  

> diz[“domani”] += 1  

incrementa di 1 il valore contenuto in diz[“domani”], che può essere quindi utilizzato per contare le occorrenze della parola “domani”.  

Ultimo dettaglio relativo al programma: per pigrizia e per semplicità, il programma va eseguito una volta, per determinare i dizionari di ciascun articolo, e un’altra per calcolare il dizionario aggregato. Basta modificare il valore della variabile “dettagli” (= 0, dizionario unico, = 1 un dizionario per articolo).  


[IV]: http://www.inchiostrovirtuale.it/