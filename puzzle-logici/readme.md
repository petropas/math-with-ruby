#### Risoluzione di un enigma di Henry Dudeney

Smith, Jones e Robinson sono l’ingegnere,  il frenatore e il fuochista di un treno, ma non sono elencati esattamente in quest’ordine. Sul treno vi sono tre passeggeri con gli stessi tre nomi, identificati nelle premesse seguenti mediante un «Sig.» anteposto ai nomi.

1. Il Sig. Robinson vive a Los Angeles.
2. Il frenatore vive a Omaha.
3. Il Sig. Jones ha da parecchio tempo dimenticato tutta l’algebra imparata alle scuole superiori.
4. Il passeggero che ha lo stesso nome del frenatore vive a Chicago.
5. Il frenatore e uno dei passeggeri, un eminente fisico matematico, frequentano la stessa chiesa.
6. Smith batte il fuochista al biliardo.

**Chi è l’ingegnere?**

- dudeney-puzzle.rb
    - programma per la risoluzione dell'enigma di Dudeney
- dudeney-puzzle.txt
	- stampa del risultato

Vai all'[articolo][iv-Dudeney] su Inchiostrovirtuale.it

[iv-dudeney]:http://www.inchiostrovirtuale.it/un-enigma-ferroviario-di-henry-dudeney/

	
#### Un enigma di Barry R. Clarke

As Shaky Sheila entered the interview room, her four interrogators — Arbuthnot, Bounder, Cad, and Drat (one of whom was a non-smoker) — sat behind the oak table polishing their questions. Sheila mentally numbered them from one to four, left to right, and noted the following facts.

1. The pipe smoker sat immediately to the right of Bounder.
2. Cad did not smoke cigarettes.
3. The cigarette smoker sat immediately to the left of Arbuthnot.
4. Cad was not the cigar smoker.
5. The cigar smoker sat immediately to the left of the cigarette smoker.
6. The cigar smoker was neither second or third.

If no interrogator had two different smoking habits, can you find the name and smoking habit for each position?

Il programma esplora le soluzioni per tutte le possibili combinazioni destra-sinistra delle condizioni 1, 3 e 5.

- TheInterrogation.rb
	- programma per la risoluzione dell'enigma di Barry R. Clarke
- TheInterrogation.txt
	- stampa del risultato
	
Vai all'[articolo][iv-interrogation] su Inchiostrovirtuale.it

##### Bibliografia:
Enigma su [Challenging Logic Puzzles:][Clarke]
[Clarke]:https://books.google.it/books?id=DmY724f3jooC&pg=PA24&lpg=PA24&dq=Shaky+Sheila&source=bl&ots=_8PHNO12kr&sig=tvTctN9BJmYlQQ6pEXmO8A4VLgM&hl=it&sa=X&ved=0ahUKEwjX3Kjg-J_bAhXoIcAKHRuMBXkQ6AEIUDAI#v=onepage&q=Shaky%20Sheila&f=false

[iv-interrogation]:http://www.inchiostrovirtuale.it/matematica-in-fumo/
