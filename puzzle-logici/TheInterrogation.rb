# The Interrogation - da Puzzle logici impegnativi, di Barry R. Clarke
#
# 1) The pipe smoker sat immediately to the right of Bounder.
# 2) Cad did not smoke cigarettes.
# 3) The cigarette smoker sat immediately to the left of Arbuthnot.
# 4) Cad was not the cigar smoker.
# 5) The cigar smoker sat immediately to the left of the cigarette smoker.
# 6) The cigar smoker was neither second or third.
#
persone = ["Arbuthnot", "Bounder", "Cad", "Drat"]
fumo = ["sigaretta", "sigaro", "pipa", "niente"]
#
def uguale?(offset, uno, val_uno, due, val_due)
	case offset
		when "dx"; p = 1
		when "sx"; p = -1
		else p = 0
	end
	(0..3).any? do |i|
		next if i + p < 0
		uno[i] == val_uno && due[i+p] == val_due
	end
end
#
t0 = Time.now
["dx", "sx"].each do |c1|
["dx", "sx"].each do |c3|
["dx", "sx"].each do |c5|
#
	n_sol=0
	puts "c1 = #{c1}, c3 = #{c3}, c5 = #{c5}:\n"
	fumo.permutation.each do |abitudine|
		next unless uguale?(c5, abitudine,"sigaro", abitudine, "sigaretta") # n.ro 5
		next if abitudine[1] == "sigaro" || abitudine[2] == "sigaro" # n.ro 6
		persone.permutation.each do |disposizione|
			next unless uguale?(c1, abitudine,"pipa", disposizione, "Bounder") # n.ro 1
			next if uguale?(0, disposizione, "Cad", abitudine, "sigaretta") # n.ro 2
			next unless uguale?(c3, abitudine,"sigaretta", disposizione, "Arbuthnot") # n.ro 3
			next if uguale?(0, disposizione, "Cad", abitudine, "sigaro") # n.ro 4
			puts "#{disposizione} \n #{abitudine}\n"
			n_sol +=1
		end
	end
	puts "\n trovate #{n_sol} soluzioni  in #{(1000*(Time.now - t0)).round(2)} msec\n\n"
end
end
end
