# risoluzione di un puzzle di Dudeney
# by p.p. - aprile 2018
ferrovieri = ["Smith", "Jones", "Robinson"]
passeggeri = ["Smith", "Jones", "Robinson"]
mansioni = ["ingegnere", "frenatore", "fuochista"]
luoghi_ferr = ["LosAngeles", "Omaha", "Chicago"]
luoghi_pass = ["LosAngeles", "Omaha", "Chicago"]
#
def cond_1?(uno, val_uno, due, val_due)
	(0..3).any? do |i|
		uno[i] == val_uno && due[i] == val_due
	end
end
def cond_2?(uno, val_uno, due, val_due, tre, quattro)
	(0..3).any? do |i|
		(0..3).any? do |j|
			uno[i] != val_uno && due[j] == val_due && tre[i] == quattro[j]
		end
	end
end
def cond_3?(uno, due, tre, val_tre, quattro, val_quattro)
	(0..3).any? do |i|
		(0..3).any? do |j|
			uno[i] == due[j] && tre[j] == val_tre && quattro[i] == val_quattro
		end
	end
end
#
n_sol = 0
t0 = Time.now
mansioni.permutation.each do |ferr_mans|
	luoghi_ferr.permutation.each do |ferr_luogo|
		luoghi_pass.permutation.each do |pass_luogo|
			next unless cond_1?(passeggeri, "Robinson", pass_luogo, "LosAngeles")
			next unless cond_1?(ferr_mans, "frenatore", ferr_luogo, "Omaha")
			next unless cond_2?(passeggeri, "Jones", ferr_mans, "frenatore", pass_luogo, ferr_luogo)
			next unless cond_3?(passeggeri, ferrovieri, ferr_mans, "frenatore", pass_luogo, "Chicago")
			next  if cond_1?(ferrovieri, "Smith", ferr_mans, "fuochista")
			print "ferrovieri:\n\t",ferrovieri, "\n\t", ferr_mans, "\n\t", ferr_luogo, "\n"
			print "passeggeri:\n\t", passeggeri, "\n\t", pass_luogo, "\n---\n"
			n_sol += 1
		end
	end
end
print "\ntrovate ", n_sol, " soluzioni, in ", (1000*(Time.now - t0)).round(2), " msec\n"
